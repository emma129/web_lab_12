-- Answers to Exercise 5 here
DROP TABLE IF EXISTS appleTable_registion;

CREATE TABLE IF NOT EXISTS appleTable_registion (
  username   VARCHAR(15) NOT NULL,
  first_name VARCHAR(10),
  last_name  VARCHAR(10),
  email      VARCHAR(40),
  PRIMARY KEY (username)
);

INSERT INTO appleTable_registion (username, first_name, last_name, email) VALUES
  ('Pet', 'Peter', 'Peterson', 'dkfjs@gmail.com'),
  ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
  ('abc2', 'Emma', 'Roberts', 'ambeauty@gmail.com'),
  ('dfsd', 'Charlie', 'Brown', 'haha@snoopy.com');


SELECT *
FROM appleTable_registion