-- Answers to Exercise 6 here

DROP TABLE IF EXISTS watermelon_retalStoreTable;

CREATE TABLE IF NOT EXISTS watermelon_retalStoreTable (
  id            INT     NOT NULL AUTO_INCREMENT,
  movieName     VARCHAR(100),
  director      VARCHAR(80),
  rentalName    VARCHAR(80),
  weeklyCharge$ INT(10) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (rentalName) REFERENCES dbtest_videorentaltable (name)
);

INSERT INTO watermelon_retalStoreTable (movieName, director, rentalName, weeklyCharge$) VALUES
  ('lionKing', 'dksjfh', 'Jane Campion', '4'),
  ('toystory', 'dkjfkdf', 'Roger Donaldson', '6'),
  ('BeautyandBeast', 'snoopy', 'Michael Hurst', '2');

SELECT *
FROM watermelon_retalStoreTable



