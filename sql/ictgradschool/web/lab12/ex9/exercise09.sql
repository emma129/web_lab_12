-- Answers to Exercise 9 here

-- All information about all the members of the video store ex03
SELECT *
FROM dbtest_videorentaltable;

-- Everything about all the members of the video store, except the number of video hires they have  ex03
SELECT
  name,
  gender,
  yearOfBorn,
  yearofJoined
FROM dbtest_videorentaltable;

-- All the titles to the articles that have been written ex04
SELECT title
FROM dbtest_articleTable;

-- All the directors of the movies the video store has (without repeating any names). ex06
SELECT DISTINCT director
FROM watermelon_retalStoreTable;

-- All the video titles that rent for $2 or less a week.ex06
SELECT movieName
FROM watermelon_retalStoreTable
WHERE weeklyCharge$ <= 2;

-- A sorted list of all the usernames that have been registered.ex05
SELECT username
FROM appleTable_registion
ORDER BY username;

-- All the usernames where the user’s first name starts with the letters ‘Pete’.
SELECT username
FROM appleTable_registion
WHERE first_name LIKE 'Pete%';

-- All the usernames where the user’s first name or last name starts with the letters ‘Pete’.
SELECT username
FROM appleTable_registion
WHERE last_name LIKE 'Pete%' OR first_name LIKE 'Pete%';