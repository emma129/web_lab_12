-- Answers to Exercise 7 here
DROP TABLE IF EXISTS article_commentsTable;

CREATE TABLE IF NOT EXISTS article_commentsTable (
  id       INT NOT NULL AUTO_INCREMENT,
  title    VARCHAR(100),
  comments VARCHAR(1000),
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES dbtest_articleTable (id)
);

INSERT INTO article_commentsTable (title, comments) VALUES
  ('amazing', 'pellentesque turpis faucibus, ut vehicula felis elementum. Integer elit ex, suscipit sed ornare eget'),
  ('sadStory', ' penatibus et magnis dis parturient montes');

SELECT *
FROM article_commentsTable