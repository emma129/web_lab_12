-- Answers to Exercise 4 here
DROP TABLE IF EXISTS dbtest_articleTable;

CREATE TABLE IF NOT EXISTS dbtest_articleTable (
  id      INT NOT NULL AUTO_INCREMENT,
  title   VARCHAR(40),
  content VARCHAR(800),
  PRIMARY KEY (id)
);

INSERT INTO dbtest_articleTable (title, content) VALUES
  ('amazing',
   'Vestibulum eu dolor id metus tempus eleifend sit amet vel tellus. Vestibulum et lectus vulputate est malesuada commodo. In eget ex metus. Sed dapibus sed arcu ut feugiat. Nunc a magna a tortor ornare efficitur at vitae ipsum. Aliquam semper sem sit amet nisl lobortis tincidunt. Praesent non nunc hendrerit, bibendum felis eu, viverra mi.'),
  ('sadStory',
   'Duis facilisis nisi mauris, at interdum arcu pulvinar ut. Nullam ultrices nulla pellentesque turpis faucibus, ut vehicula felis elementum. Integer elit ex, suscipit sed ornare eget, maximus a diam. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque et mauris et nisi fringilla malesuada. Donec mattis justo augue, et fermentum dui iaculis vitae.')

SELECT *
FROM dbtest_articleTable