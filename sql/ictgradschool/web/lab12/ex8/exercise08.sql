-- Answers to Exercise 8 here

-- delete a row from exercise 05
DELETE FROM appleTable_registion
WHERE username = 'abc2';

SELECT *
FROM appleTable_registion;

-- delete a column from the same table ex05
ALTER TABLE appleTable_registion
  DROP COLUMN username;

SELECT *
FROM appleTable_registion;

-- delete the entire table ex06
DROP TABLE IF EXISTS watermelon_retalStoreTable;
DROP TABLE IF EXISTS dbtest_videorentaltable;

-- change a value that is a string ex06
UPDATE watermelon_retalStoreTable
SET movieName = 'Superman'
WHERE id = '1';

SELECT *
FROM watermelon_retalStoreTable;

-- change a value that is a numeric value ex03
UPDATE dbtest_videorentaltable
SET yearOfBorn = '2017'
WHERE name = 'Peter Jackson';

SELECT *
FROM dbtest_videorentaltable;

-- change a value that is a primary key  ex05
UPDATE appleTable_registion
SET username = 'PeterPan'
WHERE first_name = 'Bill';

SELECT *
FROM appleTable_registion;