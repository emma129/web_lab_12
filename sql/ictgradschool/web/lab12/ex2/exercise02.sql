-- Answers to Exercise 2 here
DROP TABLE IF EXISTS dbtest_registion;

CREATE TABLE IF NOT EXISTS dbtest_registion (
  username   VARCHAR(15),
  first_name VARCHAR(10),
  last_name  VARCHAR(10),
  email      VARCHAR(40),
  PRIMARY KEY (username)
);

INSERT INTO dbtest_registion (username, first_name, last_name, email) VALUES
  ('Pet', 'Peter', 'Peterson', 'dkfjs@gmail.com'),
  ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
  ('abc2', 'Emma', 'Roberts', 'ambeauty@gmail.com'),
  ('dfsd', 'Charlie', 'Brown', 'haha@snoopy.com');